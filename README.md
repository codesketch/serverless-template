# dino-template

A Typescript-based template project for serverless implementations.

# Create a template project

- Download ./bi/create.sh
- Run bash ./create.sh <your-service-name> <your-service-absolute-path>
