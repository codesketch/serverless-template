import { type ServiceService } from '../application/ServiceService'
import { Interface } from 'dino-express'

interface ServiceParameters {
  lat: number
  lon: number
}

interface ServiceDependencies {
  serviceService: ServiceService
}

export class ServiceInterface extends Interface {
  private readonly serviceService: ServiceService

  constructor({ serviceService }: ServiceDependencies) {
    super()
    this.serviceService = serviceService
  }

  public search({ lat, lon }: ServiceParameters): any[] {
    console.log('I am the interface....')
    return this.serviceService.searchAtLocation(lat, lon)
  }
}
