// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Service } from 'dino-core'
import { type HeaderExtractor } from './HeaderExtractor'

interface Deps {
  headerExtractor: HeaderExtractor
}

export class ServiceService extends Service {
  private readonly headerExtractor: HeaderExtractor

  constructor({ headerExtractor }: Deps) {
    super()
    this.headerExtractor = headerExtractor
  }

  public searchAtLocation(lat: number, lon: number): any[] {
    const headers = this.headerExtractor.getHeaders()
    return [{ name: `POI 1 - ${headers['x-project']}`, location: { lat, lon } }]
  }
}
