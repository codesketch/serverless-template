import { RequestScopedInjectable } from 'dino-express'

export class HeaderExtractor extends RequestScopedInjectable {
  getHeaders(): any {
    return this.getHttpContext().headers
  }
}
