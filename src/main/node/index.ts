// Copyright 2021 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { ObjectHelper } from 'dino-core'
import { Runner } from 'dino-express'

if (ObjectHelper.isNotDefined(process.env.DINO_CONTEXT_ROOT)) {
  process.env.DINO_CONTEXT_ROOT = 'dist/interfaces,dist/infrastructure,dist/application'
}
if (ObjectHelper.isNotDefined(process.env.DINO_CONFIG_PATH)) {
  process.env.DINO_CONFIG_PATH = 'src/main/resources/dev.config.json'
}

const runner = new Runner()
void runner.load()
exports.handler = runner.run.bind(runner)
