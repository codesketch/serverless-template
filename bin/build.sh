TYPE=${1:-'lambda'}
ORGANISATION=${2:-'dino'}
NAME=$(node -p "require('./package.json').name")
VERSION=$(node -p "require('./package.json').version")

npm set progress=false && npm config set depth 0 && npm cache clean --force
npm install --prod --only=prod
npm run build

if [ "$TYPE" = "local" ]; then
  # allows to execute npm install and npm build only
  exit 0
fi
if [ "$TYPE" = "lambda" ]; then
  EXIT_CODE=$(docker build -f Dockerfile.lambda -t ${ORGANISATION}/${NAME}:${VERSION} .)
  exit $EXIT_CODE
fi
if [ "$TYPE" = "service" ]; then
  EXIT_CODE=$(docker build -f Dockerfile -t ${ORGANISATION}/${NAME}:${VERSION} .)
  exit $EXIT_CODE
fi

echo "Unknown build type ${TYPE}, corrently supported are lambda and service "
exit 1