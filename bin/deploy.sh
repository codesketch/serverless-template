DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ENV=${1:-'development'}
CLOUD_PROVIDER=${2:-'aws'}
AUTH_TOKEN=$3

NAME=$(node -p "require('./package.json').name")
VARS="-var=environment=$1 -var=name=$NAME -var=auth_token=${AUTH_TOKEN}"

do_execute_terraform() {
  echo "moving to $1 directory"
  cd "$1"
  echo "moving to $2 workspace"
  terraform workspace new $2 | true
  terraform workspace select $2
  echo "initialising to $2 workspace"
  terraform init
  echo "executing terraform"
  terraform apply -auto-approve $3
}

cd $DIR/../
if [ "$CLOUD_PROVIDER" = "gcp" ]; then
  echo "preparing for release"
  do_execute_terraform "${CURRENT_DIRECTORY}/terraform/$CLOUD_PROVIDER" $ENV $VARS
else
  CURRENT_DIRECTORY="$(pwd)"
  VERSION=$(node -p "require('./package.json').version")
  VARS="$VARS -var=version=$VERSION -var=context_directory=${CURRENT_DIRECTORY}"
  echo "Building docker image"
  do_execute_terraform "${CURRENT_DIRECTORY}/terraform/$CLOUD_PROVIDER/docker" $ENV $VARS
  echo "Deploying docker image to Lambda Function"
  do_execute_terraform "${CURRENT_DIRECTORY}/terraform/$CLOUD_PROVIDER/service" $ENV $VARS
fi

