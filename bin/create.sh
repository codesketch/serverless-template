#!/bin/bash

NAME=${1}
DESTINATION=${2:-"."}
if [ -z ${$NAME+x} ]; then
  echo "Project location must be provided bash create.sh <my-project-name>"
  exit 1
fi

cd /tmp
echo "Cloning dino-template to ${DESTINATION}"
git clone https://gitlab.com/codesketch/dino-template.git ${DESTINATION}

cd ${DESTINATION}
sed -i '' -e "s|dino-template|${NAME}|" package.json
echo ${NAME} > README.md
sed -i '' -e "s|Business service|'${NAME}'|" src/main/resources/routes.yml

npm update
npm i
npm run build
npm start