FROM --platform=linux/x86_64 nodejs:20

LABEL maintainer="Quirino Brizi <quirino.brizi@gmail.com>"

ENV DINO_CONFIG_PATH="${LAMBDA_TASK_ROOT}/resources/dev.config.json"

ADD package.json ${LAMBDA_TASK_ROOT}/
ADD ./dist ${LAMBDA_TASK_ROOT}/dist
ADD ./node_modules ${LAMBDA_TASK_ROOT}/node_modules
ADD ./src/main/resources ${LAMBDA_TASK_ROOT}/resources
RUN ln -s ${LAMBDA_TASK_ROOT}/dist/index.js ${LAMBDA_TASK_ROOT}/index.js

CMD ["index.handler"]