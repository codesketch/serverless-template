locals {
  timestamp   = formatdate("YYMMDDhhmmss", timestamp())
}

data "archive_file" "archive" {
    type        = "zip"
    source_dir  = "../../dist"
    output_path = "../../dist/${var.name}.zip"
}

resource "google_storage_bucket" "bucket" {
  name = "${var.name}-bucket"
  location      = "EU"
  project = var.project_id
  force_destroy = true
}

resource "google_storage_bucket_object" "archive" {
  name   = "${var.name}_${local.timestamp}.zip"
  bucket = google_storage_bucket.bucket.name
  source = "../../dist/${var.name}.zip"
}


resource "google_cloudfunctions_function" "function" {
  name        = var.name
  description = "${var.environment}-${var.name}"
  runtime     = "nodejs14"

  available_memory_mb   = ver.memory_mb
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  environment_variables = {
    "ENVIRONMENT" = var.environment
    "LOG_LEVEL" = var.log_level
    "CLOUD_PROVIDER" = "google"
    "AWS_NODEJS_CONNECTION_REUSE_ENABLED"= "1"
  }
  entry_point = var.name
  ingress_settings = "ALLOW_ALL"
  timeout = 60

  region = var.region
  project = var.project_id
  max_instances = var.max_instances
}




