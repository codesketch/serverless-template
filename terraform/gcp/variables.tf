variable "name" {
  type        = string
  description = "The name of the function"
}

variable "project_id" {
  type        = string
  description = "The project ID to host the cluster in."
}

variable "region" {
  type        = string
  description = "The region to host the cluster in."
}

variable "credentials" {
  type        = string
  description = "The credential to use to operate"
}

variable "environment" {
  type        = string
  description = "the environment to deploy on"
}

variable "memory_mb" {
  type        = number
  description = "the memory to assign to the function"
}

variable "log_level" {
  type        = string
  description = "the log level for the function"
  default = "info"
}

variable "max_instances" {
  type = number
  description = "the number of function that may coexist at a given time"
}