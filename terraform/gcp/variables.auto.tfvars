credentials        = "./terraform-iaac-keyfile.json"
project_id         = "my-project-id"
region             = "europe-west1"
name               = "serverless_template"
memory_mb          = 128
max_instances      = 1