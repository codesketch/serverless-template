resource "aws_security_group" "general_security_group" {
  name   = "${var.environment}_${var.name}_general_security_group"
  vpc_id = var.vpc_id

  egress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
}

resource "aws_sqs_queue" "dlq" {
  name = "${var.environment}_${var.name}_dlq"
}

data "aws_iam_policy_document" "general_lambda_policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:eu-west-1:840186222148:*"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "ec2:DescribeNetworkInterfaces",
      "ec2:CreateNetworkInterface",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeInstances",
      "ec2:AttachNetworkInterface"
    ]
    resources = [
      "*"
    ]
  }
}

data "aws_caller_identity" "this" {}
data "aws_region" "current" {}
data "aws_ecr_authorization_token" "token" {}
locals {
  ecr_address = format("%v.dkr.ecr.%v.amazonaws.com", data.aws_caller_identity.this.account_id, data.aws_region.current.name)
  ecr_image   = format("%v/%v:%v", local.ecr_address, var.name, var.version)
}


module "lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "1.43.0"

  function_name = "${var.environment}_${var.name}"
  description   = "aws:states:opt-in - ${var.environment}_${var.name}"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  timeout       = 30
  memory_size   = var.memory_mb

  lambda_at_edge = false
  create_package = false

  ##################
  # Container Image
  ##################
  image_uri    = local.ecr_image
  package_type = "Image"
  reserved_concurrent_executions = var.max_instances

  # // Attach a policy.
  attach_policy_json = true
  policy_json = data.aws_iam_policy_document.general_lambda_policy.json

  #  Add environment variables.
  environment_variables = {
    ENVIRONMENT = var.environment
    STAGE       = var.environment
    LOG_LEVEL   = var.log_level
  }

  // Deploy into a VPC.
  vpc_subnet_ids         = var.subnet_ids
  vpc_security_group_ids = [
    aws_security_group.general_security_group.id
  ]

  dead_letter_target_arn = aws_sqs_queue.dlq.arn
}

