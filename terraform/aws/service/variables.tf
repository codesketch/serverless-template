variable "vpc_id" {
  type = string
  description = "The VPC identifier"
}

variable "subnet_ids" {
  type = list(string)
  description = "The list of subnets inside the VPC"
}

variable "environment" {
  type        = string
  description = "the environment to deploy on"
}

variable "memory_mb" {
  type        = number
  description = "the memory to assign to the function"
}

variable "log_level" {
  type        = string
  description = "the log level for the function"
  default = "info"
}

variable "max_instances" {
  type = number
  description = "the number of function that may coexist at a given time"
}

variable "version" {
  type = string
  default = "0.0.1"
}

variable "name" {
  type = string
  default = "serverless-template"
}

variable "region" {
  type        = string
  description = "The region to host the cluster in."
}