#!/bin/sh

set -e

IMAGE=$1
CONTEXT=$2
AUTH_TOKEN=$3

echo "Building $IMAGE on $CONTEXT"

docker build --build-arg CI_JOB_TOKEN=${CI_JOB_TOKEN} -t $IMAGE $CONTEXT 