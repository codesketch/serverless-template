variable "name" {
  type = string
  default = "sseerverless-template"
}

variable "version" {
  type = string
  default = "1.0"
}

variable "context_directory" {
  type = string
  default = "../../../../"
  description = "The path to the directory that contains the Dockerfile"
}

variable "auth_token" {
  type = string
  description = "NPM registry authentication token"
}