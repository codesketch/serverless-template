#################
# ECR Repository
#################
resource "aws_ecr_repository" "this" {
  name = var.name
}

###############################################
# Create Docker Image and push to ECR registry
###############################################

data "aws_caller_identity" "this" {}
data "aws_region" "current" {}
data "aws_ecr_authorization_token" "token" {}

locals {
  ecr_address = format("%v.dkr.ecr.%v.amazonaws.com", data.aws_caller_identity.this.account_id, data.aws_region.current.name)
  ecr_image   = format("%v/%v:%v", local.ecr_address, aws_ecr_repository.this.id, var.version)
}

resource "null_resource" "build" {
  triggers = {
    always_run = "${timestamp()}"
  }

  # See build.sh for more details
  provisioner "local-exec" {
    command = "${path.root}/bin/build.sh ${local.ecr_image} ${var.context_directory} ${var.auth_token}"
  }
}

resource "docker_registry_image" "app" {
  name = local.ecr_image
  depends_on = [ null_resource.build ]
}