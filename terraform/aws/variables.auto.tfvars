vpc_id             = "VPC_ID"
subnet_ids         = ["my_subnet_ids"]
region             = "eu-west1"
name               = "serverless_template"
memory_mb          = 128
max_instances      = 1